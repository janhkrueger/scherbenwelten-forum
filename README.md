# scherbenwelten-forum

Backup of the old scherbenwelten.de forum before closure


## Migrated
Warning. This repository has moved to: https://github.com/janhkrueger/scherbenwelten-forum

Further changes will only be made there.